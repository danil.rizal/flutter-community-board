import 'package:flutter/material.dart';
import 'package:s27_firebase_intro/app/home.dart';
import './app/login.dart';

void main() => runApp(new MainApp());

class MainApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Whita Community Board",
      home: LoginScreen(),
    );
  }
}