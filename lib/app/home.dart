import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';
import 'package:s27_firebase_intro/model/board.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Board> messages = List();
  Board board;
  final FirebaseDatabase database = FirebaseDatabase.instance;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  DatabaseReference databaseReference;

  void initState() {
    super.initState();
    board = Board("", "");
    databaseReference = database.reference().child("community_board");
    databaseReference.onChildAdded.listen(_onEntryAdded);
    databaseReference.onChildChanged.listen(_onEntryChanged);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Community Board"),
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            flex: 0,
            child: Container(
              padding: EdgeInsetsDirectional.only(bottom: 20.0),
              color: Colors.grey.shade200,
              child: Form(
                key: formKey,
                child: Flex(
                  direction: Axis.vertical,
                  children: <Widget>[
                    ListTile(
                      leading: Icon(Icons.subject),
                      title: TextFormField(
                          decoration: InputDecoration(labelText: "Subject"),
                          initialValue: "",
                          onSaved: (val) => board.subject = val,
                          validator: (val) => val == "" ? val : null),
                    ),
                    ListTile(
                      leading: Icon(Icons.message),
                      title: TextFormField(
                        decoration: InputDecoration(labelText: "Message"),
                        initialValue: "",
                        onSaved: (val) => board.message = val,
                        validator: (val) => val == "" ? val : null,
                      ),
                    ),
                    Container(
                      padding: EdgeInsetsDirectional.only(top: 16.0),
                      child: RaisedButton(
                        child: Text("POST"),
                        color: Colors.redAccent,
                        // onPressed: () {}
                        onPressed: _handleSubmit,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Flexible(
            child: FirebaseAnimatedList(
              query: databaseReference,
              itemBuilder: (_, DataSnapshot snapshot,
                  Animation<double> animation, int index) {
                return Card(
                  child: ListTile(
                    leading: CircleAvatar(
                      child: Text(
                        messages[index].subject[0],
                        style: TextStyle(color: Colors.white),
                      ),
                      backgroundColor: Colors.red,
                    ),
                    title: Text(messages[index].subject),
                    subtitle: Text(messages[index].message),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }

  void _onEntryAdded(Event event) {
    setState(() {
      messages.add(Board.fromSnapshot(event.snapshot));
    });
  }

  void _handleSubmit() {
    final FormState form = formKey.currentState;
    if (form.validate()) {
      form.save();
      form.reset();
      databaseReference.push().set(board.toJson());
    }
  }

  void _onEntryChanged(Event event) {
    var oldEntry = messages.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });

    setState(() {
      messages[messages.indexOf(oldEntry)] = Board.fromSnapshot(event.snapshot);
    });
  }
}
