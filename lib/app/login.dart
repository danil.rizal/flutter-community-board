import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'dart:async';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = new GoogleSignIn();
  String _imageUrl;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
        centerTitle: true,
      ),
      body: Center(
        child: ListView(
          children: <Widget>[
            RaisedButton(
                child: Text(
                  "Google",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.red,
                onPressed: () => _gSignIn()),
            RaisedButton(
                child: Text("Email Signin"),
                color: Colors.orange,
                onPressed: () => _emailSignIn()),
            RaisedButton(
                child: Text("Create Email"),
                color: Colors.yellow.shade300,
                onPressed: () => _createUserAccount()),
            RaisedButton(
                child: Text("Signout"),
                color: Colors.red,
                onPressed: () => _signOut()),
            Image.network(_imageUrl == null || _imageUrl.isEmpty
                ? "https://ultrabasesystems.com/wp-content/uploads/2015/10/placeholder-Copy.png"
                : _imageUrl)
          ],
        ),
      ),
    );
  }

  Future<FirebaseUser> _gSignIn() async {
    GoogleSignInAccount account = await _googleSignIn.signIn();
    GoogleSignInAuthentication googleSignInAuthentication =
        await account.authentication;

    FirebaseUser user = await _auth.signInWithGoogle(
        idToken: googleSignInAuthentication.idToken,
        accessToken: googleSignInAuthentication.accessToken);

    print("User: ${user.displayName}\nPhoto: ${user.photoUrl}");
    setState(() {
      _imageUrl = user.photoUrl;
    });

    return user;
  }

  Future _createUserAccount() async {
    FirebaseUser user = await _auth
        .createUserWithEmailAndPassword(
            email: "danil@whitaaplikasi.com", password: "whita123")
        .then((user) {
      print("Username: ${user.email}");
    });
  }

  _signOut() async {
    final status = await _googleSignIn.isSignedIn();
    if (status) {
      setState(() {
        _googleSignIn.signOut();
        _imageUrl = null;
      });
      print("Gmail signed out!");
    } else {
      setState(() {
        _auth.signOut();
        _imageUrl = null;
      });
      print("Email account signed out!");
    }
  }

  _emailSignIn() async {
    await _auth
        .signInWithEmailAndPassword(
            email: "danil@whitaaplikasi.com", password: "whita123")
        .catchError((error) {
      print("Something went wrong!: $error");
    }).then((user) {
      print("User logged in: ${user.email}");
      setState(() {
        _imageUrl =
            "http://icons.iconarchive.com/icons/graphicloads/100-flat/256/email-2-icon.png";
      });
    });
  }
}
