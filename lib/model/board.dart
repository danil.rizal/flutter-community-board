import 'package:firebase_database/firebase_database.dart';

class Board {
  String key;
  String subject;
  String message;

  Board(this.subject, this.message);

  Board.fromSnapshot(DataSnapshot snapshot) :
    key = snapshot.key,
    subject = snapshot.value['subject'],
    message = snapshot.value['message'] ; 

  toJson() {
    return {
      'subject': subject,
      'message': message
    };
  }
}